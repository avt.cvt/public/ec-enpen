// mesh element size
lc = 1; // one micrometer
//depletion = 2000;
enrichment = 2000;
outer = 50;
memb_thickness = 200;


Point(1) = {0.0, 0.0, 0.0, lc};
Point(2) = {outer, 0.0, 0.0, lc};
Point(3) = {outer+memb_thickness, 0.0, 0.0, lc};
Point(4) = {outer+memb_thickness+enrichment, 0.0, 0.0, lc};
Point(5) = {outer+memb_thickness+enrichment+memb_thickness, 0.0, 0.0, lc};
Point(6) = {outer+memb_thickness+enrichment+memb_thickness+outer, 0.0, 0.0, lc};
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,6};

Field[1] = BoundaryLayer;
Field[1].NodesList = {2, 3, 4, 5};
Field[1].hfar = lc;
Field[1].hwall_n = lc/1000;
Field[1].hwall_t = lc/1000;
Field[1].ratio = 1.01;

Background Field = 1;
