// mesh element size
lc = 1/1; // one micrometer
lc_fine = lc/1000000000;
h = 60; // length of refinement
thickness = 100.0; // boundary layer thickness [1mu]
memb_thickness = 100.0; // membrane thickness [1mu]
Point(1) = {0.0, 0.0, 0.0, lc_fine};
Point(2) = {thickness, 0.0, 0.0, lc};
Point(3) = {thickness+memb_thickness, 0.0, 0.0, lc};
Point(4) = {thickness+memb_thickness+thickness, 0.0, 0.0, lc_fine};

Point(5) = {h,0,0,lc};
Point(6) = {thickness+memb_thickness+thickness-h,0,0,lc};


Line(1) = {1,5};
Line(4) = {5,2};
Line(2) = {2,3};
Line(5) = {3,6};
Line(3) = {6,4};
