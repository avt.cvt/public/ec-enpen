#!/usr/bin/env python3
import matplotlib
import matplotlib.pyplot as plt
import json
import sys
import math
from matplotlib import animation

class NPPplot:

	F = 96485.3
	R = 8.31446
	T = 298.15
	anim = []

	def __init__(self,experiment):
			filename = 'outdata/' + experiment + '.json'

			self.data = self.read_file(filename)
			self.nv = self.data["meta"]["nv"]
			self.charge = self.data["meta"]["charge"]
			self.names = self.data["meta"]["species"]
			self.D = self.data["meta"]["diffcoeff"]

			keys = list(self.data.keys())
			keys = keys[1:len(keys)-1]

			self.results = [{}]*len(keys)
			self.fluxes = [{}]*len(keys)
			for j in keys:
				self.results[int(j)] = {"x":[],"y":[[0]]*self.nv,"indexval":0}
				self.results[int(j)]["indexval"] = self.data[j]["indexval"]
				self.fluxes[int(j)] = {"x":[],"y":[[0]]*self.nv,"indexval":0}
				self.fluxes[int(j)]["indexval"] = self.data[j]["indexval"]
				for i in range(self.nv):
					[self.results[int(j)]["x"],self.results[int(j)]["y"][i]] \
						= self.get_xplot(self.data, j, "stt", i)
					[self.fluxes[int(j)]["x"],self.fluxes[int(j)]["y"][i]] \
						= self.get_xplot(self.data, j, "flx", i)


	def read_file(self,filename):
		f = open(filename,'r')
		try: 
			return json.load(f)
		except:
			return json.loads('{"empty":[]}')

	def diagnosis(self,data):
		indices = []
		x = []
		y = []
		z = []
		for idx in data:
			if idx == "empty":
				continue
			indices.append(float(idx))
			states = data[idx]["stt"]
			for s in states:
				x.append(s["x"])
				y.append(s["y"])
				z.append(s["z"])
		print("idx %f - %f" % (min(indices), max(indices)))
		print("x %f - %f" % (min(x), max(x)))
		print("y %f - %f" % (min(y), max(y)))
		print("y %f - %f" % (min(z), max(z)))

	def sort(self,l1, l2):
		return (list(t) for t in zip(*sorted(zip(l1, l2))))

	def get_xplot(self,data, index, setname, vecidx):
		x = []
		y = []
		for dataset in data[index][setname]:
			x.append(dataset["x"])
			y.append(dataset["v"][vecidx])
		return self.sort(x, y)

	def get_emp(self,data, index):
		x = []
		y = []
		for dataset in data["%.3f"%index]["stt"]:
			x.append(dataset["x"])
			y.append(dataset["emp"])
		return self.sort(x, y)

	def get_iyplot(self,data, volidx, setname, vecidx):
		x = []
		y = []
		for dataset in data:
			if dataset == "empty":
				continue
			x.append(float(dataset))
			d = data[dataset][setname][volidx]
			y.append(d["v"][vecidx])
		return self.sort(x, y)
	def get_idx_at(self,x, x0):
		for i, v in enumerate(x):
			if x0 < v:
				return i

	def plot_spec(self,index):
		fig_spec = plt.figure()
		plots = []
		ax = plt.axes()
		for i in range(1,self.nv-1):
			plot, = ax.plot(self.results[index]["x"], self.results[index]["y"][i])
			plots.append(plot)
		plt.legend(plots, self.names[1:self.nv-1])
		plt.ylabel("Concentration")
		plt.xlabel('Space [$\mu$m]')

	def plot_phi(self,index):
		fig_phi = plt.figure()
		phi, = plt.plot(self.results[index]["x"], self.results[index]["y"][self.nv-1])
		plt.legend([phi], [self.names[self.nv-1]])

		plt.xlabel('Space [$\mu$m]')
		plt.ylabel('Potential')

	def rho(self,index):
		r = [0] * len(self.results[index]["x"])
		for i in range(1,self.nv-1):
			for j,v in enumerate(self.results[index]["y"][i]):
				r[j] += v*self.charge[i]
		return r

	def plot_rho(self,index):
		plt.figure()
		plt.plot(self.results[index]["x"],self.rho(index))
		plt.xlabel('Space')
		plt.ylabel(r'$\rho$')

	def plot_flux(self,index):
		fig_flx = plt.figure()
		plots = []
		ax = plt.axes()
		for i in range(1,self.nv-1):
			plot, = ax.plot(self.fluxes[index]["x"], self.fluex[index]["y"][i])
			plots.append(plot)
		plt.legend(plots, self.names[1:self.nv-1])
		plt.ylabel("Flux")
		plt.xlabel('Space [$\mu$m]')

	def animate(self,plot,species,frame):
		r = self.results[frame]
		plot.set_data(r["x"],r["y"][species])

	def animate_species(self,species):
		anim_fig = plt.figure()
		anim_ax = plt.axes()
		plot, = anim_ax.plot(self.results[len(self.results)-1]["x"],self.results[len(self.results)-1]["y"][species])
		plot_1, = anim_ax.plot(self.results[1]["x"],self.results[1]["y"][species])
		plot_end, = anim_ax.plot(self.results[len(self.results)-1]["x"],self.results[len(self.results)-1]["y"][species])
		plt.legend(['animated','$t_1$','$t_{end}$'])
		plt.xlabel('Space [$\mu$m]')
		plt.title("Species %d"%species)
		self.anim.append( animation.FuncAnimation(anim_fig, lambda frame: self.animate(plot,species,frame),
			frames=len(self.results), interval=20, blit=False) )

	def animate_f(self,plot,species,frame):
		r = self.fluxes[frame]
		plot.set_data(r["x"],r["y"][species])

	def animate_flux(self,species):
		anim_fig = plt.figure()
		anim_ax = plt.axes()
		plot, = anim_ax.plot(self.fluxes[len(self.fluxes)-1]["x"],self.fluxes[len(self.fluxes)-1]["y"][species])
		plot_1, = anim_ax.plot(self.fluxes[1]["x"],self.fluxes[1]["y"][species])
		plot_end, = anim_ax.plot(self.fluxes[len(self.fluxes)-1]["x"],self.fluxes[len(self.fluxes)-1]["y"][species])
		plt.legend(['animated','$t_1$','$t_{end}$'])
		plt.xlabel('Space [$\mu$m]')
		plt.title("Flux species %d"%species)
		self.anim.append( animation.FuncAnimation(anim_fig, lambda frame: self.animate_f(plot,species,frame),
			frames=len(self.results), interval=20, blit=False) )

	def save_animation(self,index,file):
		self.anim[index].save('plots/' + file, fps=10, extra_args=['-vcodec', 'libx264'],dpi=300)
		 

if __name__ == "__main__" and len(sys.argv) >= 2:

	V = 0
	if len(sys.argv) == 3:
		V = int(sys.argv[2])
	d = NPPplot(sys.argv[1])

	print(d.nv)

	for i in range(0,d.nv):
		d.animate_flux(i)
		d.animate_species(i)
	plt.show()

