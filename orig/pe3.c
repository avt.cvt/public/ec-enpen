#include "app.h"
#include "iface_petsc.h"
#include "modules.h"
#include "parameters.h"
#include "mesh.h"
#include "simulation.h"
#include "equi.h"
#include "petscdraw.h"

int main(int argc, char **argv)
{
    int err;
    interface_t *iface = &IFACE_PETSC;
    err = iface->init(&argc, &argv); if(err) return err;

    /* Set up simulation */
    parameter_t p;
    mesh_t mesh;
    app_t app = {.mesh=&mesh, .p=&p, .solverdata = (void *) &NPP_WALLBC};
    // app_t app = {.mesh=&mesh, .p=&p, .solverdata = (void *) &NPP_1D_REAC_TS};
    param_norm_t norm = {.c0=1000,.D0=1.0e-9}; /* Molar scale (c0=1000) */
    // param_set(&p, "sysdata/pe3nr.def", norm);
    param_set(&p, "sysdata/simple.def", norm);

    meshdef_t def = TEST;
    // meshdef_t def = TRILAYER;
    // meshdef_t def = UNIFORM;
    mesh_set(&mesh, &def);

    unsigned int n = 4; //p.num_species + NPP_1D.num_add_vars;
    unsigned int i;

    // double initial[8] = {1,1,1,1,1,1,1,0};
    double initial[4] = {1,1,1,0};

    /* Set up regions */
    zone_t ppp = PE3_PDOTPSSPEI;
    ppp.epsilon = 1;
    ppp.alpha = 1;
    ppp.ion_block = calloc(n, sizeof(int));
    for(i = 0; i<n; i++) ppp.ion_block[i] = 1;
    ppp.end_charge = 0; //todo: figure out charges
    err = mesh_add_zone(&mesh, &ppp); if(err) return err;

    zone_t nafion = PE3_NAFION;
    nafion.epsilon = 1;
    nafion.alpha = 1;
    nafion.ion_block = calloc(n, sizeof(int));
    for(i = 0; i<n; i++) nafion.ion_block[i] = 1;
    nafion.end_charge = 0; // aem.charge*norm.c0 = [mol/m^3]
    err = mesh_add_zone(&mesh, &nafion); if(err) return err;

    zone_t pp = PE3_PDOTPSS;
    pp.epsilon = 1;
    pp.alpha = 1;
    pp.ion_block = calloc(n, sizeof(int));
    for(i = 0; i<n; i++) pp.ion_block[i] = 1;
    pp.end_charge = 0;
    err = mesh_add_zone(&mesh, &pp); if(err) return err;

    /* Load application */
    err = iface->load(&app); if(err) return err;

    mesh_set_state(&mesh, &initial[0], n);
    boundary_set_state(&mesh, FACETYPE_BOUNDARY_RIGHT, &initial[0], n);
    boundary_set_state(&mesh, FACETYPE_BOUNDARY_LEFT, &initial[0], n);

    copy_solution_from_mesh(&app);
  
    /* Run experiments */
    err = app_set_name(&app, "PE3"); if(err) return err;
    // err = sweep_boundary_chargeall(&app, FACETYPE_BOUNDARY_RIGHT, initial,
    //     &initial[p.num_species], 0.00, 10, 5); if(err) return err;

    //err = time_current_control(&app, initial, n); if(err) return err;


    // Set Buttler Volmer current
    // double j;
    // j = 1e-2*(pow(2.7183,0.5*p.F/p.R/p.T*0.1)- pow(2.7183, -1*0.5*p.F/p.R/p.T*0.1));
    // printf("Current density: %f A/m^2\n", j);

    volumelist_t *head = (&app)->mesh->boundaries[FACETYPE_BOUNDARY_LEFT];
    while(head) {
        for (i = 0; i < n-1; i++) {
            head->volume->bcond_type[i] = 2;
            head->volume->boundary_flux[i] = 0;
        }   
        // head->volume->boundary_flux[6] = j;
        head = head->next;
    }

    head = (&app)->mesh->boundaries[FACETYPE_BOUNDARY_RIGHT];
    while(head) {
        for (i = 0; i < n-1; i++) {
            head->volume->bcond_type[i] = 2;
            head->volume->boundary_flux[i] = 0;
        }   
        // head->volume->boundary_flux[6] = -1*j;
        head = head->next;
    }

    boundary_modify_state(app.mesh, FACETYPE_BOUNDARY_RIGHT, 1e-1, app.p->num_species);
    boundary_modify_state(app.mesh, FACETYPE_BOUNDARY_LEFT, -1e-1, app.p->num_species);
    transient_constant_voltage(&app, &initial[0], n);

    // sweep_zones(&app);
    // snes_solve(&app);

    /* Finish and exit */
    return iface->finish(&app);
}
