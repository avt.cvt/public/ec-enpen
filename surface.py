# %%
from py.Npp1D import Npp1D
import matplotlib.pyplot as plt
import pandas as pd

plt.style.use('py/cvt.mplstyle')

# %% Load data
d = Npp1D('outdata/cathode')

times = d.times
potentials = d.timeEvolution['voltage']
fluxes = d.timeEvolution['fluxes']

def getsurface(species):
    return [d.getstate(i, max_rows=5)[0, species] for i,_ in enumerate(times)]

faraday = 96485.3321

df = pd.DataFrame({
    'time': times,
    'potential': potentials,
    'CO2': getsurface(d.names.index('CO2')),
    'OH-': getsurface(d.names.index('OH-')),
    'H+': getsurface(d.names.index('H+')),
    'K+': getsurface(d.names.index('K+')),
    'CO3-2': getsurface(d.names.index('CO3-2')),
    'HCO3-': getsurface(d.names.index('HCO3-')),
    'phi': getsurface(6), 
    'current': fluxes[:,-1] * -1 * faraday / 10
})

# %% OH- and CO2
plt.plot(df.time, df['OH-'] * 1000, label='OH-')
plt.plot(df.time, df['CO2'] * 1000, label='CO2')
plt.xlabel('Time (s)')
plt.ylabel('Concentration (mmol/l)')
plt.yscale('log')
plt.legend()
plt.tight_layout()
plt.savefig('plots/cathode-surface.png')

# %% Current
plt.plot(df.time, df.current)
plt.xlabel('Time (s)')
plt.ylabel('Current density (mA/cm2)')
plt.xlim(9,15)
plt.tight_layout()
plt.savefig('plots/cathode-current.png')


# %% All concentrations log
plt.plot(df.time, df['OH-'] * 1000, label='OH-')
plt.plot(df.time, df['CO2'] * 1000, label='CO2')
plt.plot(df.time, df['K+'] * 1000, label='K+')
plt.plot(df.time, df['H+'] * 1000, label='H+')
plt.plot(df.time, df['CO3-2'] * 1000, label='CO3-2')
plt.plot(df.time, df['HCO3-'] * 1000, label='HCO3-')
plt.xlabel('Time (s)')
plt.ylabel('Concentration (mmol/l)')
plt.yscale('log')
plt.legend(bbox_to_anchor=(1.05, 1))


# %% Phi OHP
plt.plot(df.time, df.phi)
plt.xlabel('Time (s)')
plt.ylabel('Potential OHP (Vth)')