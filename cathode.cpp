#ifndef PETSC_CLANGUAGE_CXX
#define PETSC_CLANGUAGE_CXX
#endif
#include <petscsys.h>

#include "src/ParameterStore.h"
#include "src/Application.h"
#include "src/Models/GMPNP.h"
#include "src/Mesh/Mesh1D.h"
#include "src/ResultWriter/ResultWriter.h"
#include "src/ResultWriter/ResultLoader.h"
#include <string>
#include <map>
#include "math.h"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <list>
#include "src/Reactions/ECReaction.h"

int main(int argc, char **argv)
{
    PetscErrorCode ierr;
    ierr = PetscInitialize(&argc, &argv, NULL, NULL);
    CHKERRXX(ierr);

    double frt = 96485.3329 / (8.314472 * 298.15); // F/RT

    ParameterStore p("sysdata/carbonates.def");

    p.gmpnp_nu.resize(6, 1.0);
    p.beta.resize(6, 1.0);
    double a_i[6] = {0.56, 0.6, 0.662, 0.788, 0.8, 0.23}; // nm  [H+, OH-, K+, CO32-, HCO3-, CO2]
    double avogadro = 6.02214076;                         // 1e23 / mol
    for (unsigned int s = 0; s < 6; s++)
    {
        p.gmpnp_nu[s] = (((a_i[s] * a_i[s] * a_i[s]) * avogadro) / 10);
        p.beta[s] = pow(a_i[s] / 0.3, 3);
    }

    auto nm = p.nameMap;

    GMPNP model(&p);

    Application app(&p, (Model *)&model);
    unsigned int n = app.nv;
    double k_plus = -0.00014e-3 + 7.1e-8 + 2 * 0.039e-3 + 99.92e-3;
    double initialDEF[] = {0.00014e-3, 7.1e-8, k_plus, 0.039e-3, 99.920e-3, 34.061e-3, 0};

    Mesh1D mesh("meshdata/cathode_150thk-1D.msh", n);

    mesh.set_state(&initialDEF[0], n);

    Zone *left_zone = mesh.getZone("LEFT");
    left_zone->applyToVolumes();

    mesh.initialize();
    app.setMesh((Mesh *)&mesh);

    ECReaction co2red = ECReaction(n);
    co2red.butler_volmer = true;
    co2red.e_eq = 0.09 * frt;
    co2red.z = 2;
    co2red.j0 = 4.5e-3; // unit A/ m^2 (for Co2 reduction) 4.71e-3
    co2red.alpha = 0.68;
    co2red.i_red = 5;
    co2red.c_ref_red = 34.061e-3;
    co2red.nu[5] = -1; // consume co2
    co2red.nu[1] = 2;  // produce oh-

    ECReaction her = ECReaction(n);
    her.butler_volmer = true;
    her.e_eq = 0.2 * frt;
    her.alpha = 0.41;
    her.j0 = 3e-3;
    her.z = 2;
    her.nu[1] = 2;

    for (auto &f : left_zone->getLeftBoundary())
    {
        f->setBoundaryState(&initialDEF[0]);
        f->ecreactions.push_back(&co2red);
        f->ecreactions.push_back(&her);
    }

    for (auto &f : left_zone->getRightBoundary())
    {
        f->setBoundaryState(&initialDEF[0]);
    }

    int bcond[] = {BC_DIRICHLET, BC_DIRICHLET, BC_DIRICHLET, BC_DIRICHLET, BC_DIRICHLET, BC_DIRICHLET, BC_DIRICHLET};
    int bcond_cathode[] = {BC_NEUMANN, BC_NEUMANN, BC_NEUMANN, BC_NEUMANN, BC_NEUMANN, BC_NEUMANN, BC_SURFACE_CHARGE};

    mesh.setBoundaryCondition(left_zone->getLeftBoundary(), &bcond_cathode[0]);
    mesh.setBoundaryCondition(left_zone->getRightBoundary(), &bcond[0]);

    ResultWriter rw("outdata/cathode", &app);
    app.rw = &rw;
    mesh.setBoundaryState(left_zone->getLeftBoundary(), -0.2 * frt, n - 1);
    app.outputSamplingRate = 1e-1;
    app.transient(10, 1e-30);
    app.simtime = 10;

    for (int i = 0; i < 10; i++)
    {
        TSSetTime(app.solverdata->ts, 0);
        mesh.setBoundaryState(left_zone->getLeftBoundary(), -1.05 * frt, n - 1);
        app.transient(5e-1, 1e-30);
        app.simtime += 5e-1;

        TSSetTime(app.solverdata->ts, 0);
        mesh.setBoundaryState(left_zone->getLeftBoundary(), -0.2 * frt, n - 1);
        app.transient(5e-1, 1e-30);
        app.simtime += 5e-1;
    }

    rw.close();
    app.destroy();
    return PetscFinalize();
}
