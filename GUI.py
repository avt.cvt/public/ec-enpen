#!/usr/bin/env python3
from PyQt5.uic import loadUi, loadUiType
import sys
from PyQt5 import QtGui
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import (
    FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from PyQt5.QtWidgets import QApplication, QMainWindow, QSlider
from py.NppPlot1D import NppPlot1D as nppp,plt
import os

simulation = sys.argv[1] if len(sys.argv) > 1 else "cathode"
d = nppp('outdata/' + simulation)

class Main(QMainWindow):
    def __init__(self):
        super().__init__()
        loadUi(os.path.dirname(__file__) + '/py/GUI.ui', self)

        for name in d.names:
            self.conc_view_lst.addItem(name)
            self.flux_view_lst.addItem(name)

        self.plotType = 'conc'
        self.label = d.names[0]

        self.steps = d.get_steps()
        
        self.conc_view_lst.itemClicked.connect(self.change_concfig)
        self.flux_view_lst.itemClicked.connect(self.change_fluxfig)
        self.slider.valueChanged[int].connect(self.set_step)        
        self.slider.setMinimum(0)
        self.slider.setMaximum(self.steps)
        self.slider.setValue(0)
        self.slider.setTickPosition(QSlider.TicksBelow)
        self.slider.setTickInterval(1)
        
        self.min_label.setNum(0)
        self.max_label.setNum(self.steps)                
        
        self.fig = Figure()
        self.canvas = FigureCanvas(self.fig)
        self.mplvl.addWidget(self.canvas)
        self.canvas.draw()
        self.toolbar = NavigationToolbar(self.canvas, self.mplwindow, coordinates=True)
        self.mplvl.addWidget(self.toolbar)
        self.ax = self.fig.add_subplot(111)
        self.plot = d.animate_state_GUI(0, self.ax, 0)
        self.ax.set_xscale('log')

    def updateFig(self):
        if self.plotType == 'conc':
            d.animate_s(self.plot, d.names.index(self.label), self.slider.value())
            self.ax.set_title('State')
        else:
            d.flux_update(self.plot, d.names.index(self.label), self.slider.value())
            self.ax.set_title('Flux')
        
        self.ax.set_ylabel(self.label.upper())
        self.ax.relim()
        self.ax.autoscale_view()
        self.fig.canvas.draw()
           
    def set_step(self):
        self.num_line.setText(str(self.slider.value()))
        self.updateFig()
        
    def change_concfig(self, item):
        self.plotType = 'conc'
        self.label = item.text()
        self.updateFig()
    
    def change_fluxfig(self, item):
        self.plotType = 'flux'
        self.label = item.text()
        self.updateFig() 
        
if __name__ == '__main__':

    app = QApplication(sys.argv)
    main = Main()

    main.show()

    sys.exit(app.exec_()) 
