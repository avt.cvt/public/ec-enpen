import matplotlib.pyplot as plt
import numpy as np


def set_size(w=88, h=88, ax=None):
    """ w, h: width, height in inches """
    w *= 0.0393701
    h *= 0.0393701
    if not ax:
        ax = plt.gca()
    l = ax.figure.subplotpars.left
    r = ax.figure.subplotpars.right
    t = ax.figure.subplotpars.top
    b = ax.figure.subplotpars.bottom
    figw = float(w)/(r-l)
    figh = float(h)/(t-b)
    ax.figure.set_size_inches(figw, figh)