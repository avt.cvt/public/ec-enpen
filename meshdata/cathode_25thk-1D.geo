lc = 1/10; // 1/100 micrometer mesh size
lc_superfine_boundary = lc/1000;
h = 5; // length of refinement
thickness = 25;

Point(1) = {0.0, 0.0, 0.0, lc_superfine_boundary/1000};
Point(2) = {0.001, 0.0, 0.0, lc_superfine_boundary/2}; // {position x, position y, position z, mesh Feinheit (Volumeneinheit)}
Point(3) = {h,0,0,lc};
Point(4) = {thickness, 0.0, 0.0, lc};

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};

Physical Line ("LEFT") = {1,2,3};
