#ifndef NODE_H
#define NODE_H
#include <cmath>
#include <list>
#include <vector>

class Node{
public:
    double x,y,z;
    Node():x(0),y(0),z(0){};
    Node(double xi, double yi, double zi):x(xi),y(yi),z(zi){};
    double distanceTo(Node b);
};

#endif