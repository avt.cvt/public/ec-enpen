#ifndef BOUNDARY_H
#define BOUNDARY_H
#include "Face.h"
#include <list>

typedef enum bc_type
{
    BC_DIRICHLET = 0,
    BC_NEUMANN = 2,
    BC_SURFACE_CHARGE = 3
} bc_type_t;

class Boundary
{
    std::list<Face *> faces;

    void addFace(Face *face);
    void setBcondType(int *bcond_type);
    void setFullBcondState(double *state);
    void setBcondState(unsigned int index, double state);
};

#endif