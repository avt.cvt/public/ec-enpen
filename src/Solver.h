#ifndef SOLVERDATA_H
#define SOLVERDATA_H

#ifndef PETSC_CLANGUAGE_CXX
#define PETSC_CLANGUAGE_CXX
#endif
#include <petscsys.h>
#include <petscts.h>
#include <cmath>

#include "petscmat.h"

class Application;
class SolverData;
#include "Application.h"

#include "ParameterStore.h"

class SolverData {

public:
    Mat J; // Jacobian
    Vec x,x1,x2,r;
    // SNES snes;
    TS ts; // TimeStep
    PetscInt bs; // BlockSize
    unsigned int N;
    unsigned int num_add_vars;
    unsigned int nv;

    PetscErrorCode init(unsigned int nv, unsigned int num_variables, Application* ctx);
    void copyValuesFromMesh(Mesh* mesh);
    void copySolutionToMesh(Mesh* mesh);
    PetscErrorCode destroy();
protected:
    void create_matrix();
};
        
#endif