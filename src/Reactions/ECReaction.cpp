#include "ECReaction.h"

ECReaction::ECReaction(int num_species)
{
    if (num_species == 0)
        throw std::runtime_error("Number of species should not be 0\n");

    this->fixed_current_density.resize(num_species, 0.0);
    this->nu.resize(num_species, 0);

    this->num_species = num_species;
}