#ifndef ECREACTION_H
#define ECREACTION_H
#include <cmath>
#include <list>
#include <vector>
#include <stdexcept>
#include <cstring>

class ECReaction
{
public:
    int num_species = 0;
    std::vector<double> fixed_current_density;
    bool butler_volmer = false;
    double e_eq = 0;
    double j0 = 0;
    double alpha = 0.5;
    int i_ox = -1;
    int i_red = -1;
    double c_ref_ox;
    double c_ref_red;
    std::vector<int> nu;
    int z = 1;

    ECReaction(int num_species);
};

#endif