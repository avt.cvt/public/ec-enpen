#include "Application.h"

PetscErrorCode ts_pre_step(TS ts);
PetscErrorCode ts_post_step(TS ts);
PetscErrorCode ts_pre_step_cv(TS ts);
PetscErrorCode ts_pre_step_ramp(TS ts);
PetscErrorCode ts_pre_step_eis(TS ts);
PetscErrorCode tsmon(TS ts, PetscInt steps, PetscReal time, Vec u, void *context);

Application::Application(ParameterStore *p, Model *model)
{
    this->p = p;
    this->solverdata = new SolverData();
    this->model = model;
    this->rw = NULL;

    this->nv = this->p->num_species + this->model->num_add_vars;
    this->model->bs = this->nv;
    this->mode = "write";
}

void Application::setMesh(Mesh *mesh)
{
    if (mesh->isInitialized())
    {
        this->mesh = mesh;
        this->solverdata->init(this->nv, mesh->numVariables(), this);
        this->p->setNorm(this->mesh->mesh_unit);
    }
    else
        throw std::runtime_error("Mesh has to be initialized.");

    PetscErrorCode ierr;
    SolverData *sd = (SolverData *)this->solverdata;
    ierr = TSSetType(sd->ts, TSPSEUDO);
    CHKERRXX(ierr);
}

void Application::destroy()
{
    this->solverdata->destroy();
}

Application::~Application()
{
    delete this->solverdata;
}

PetscErrorCode ts_post_step(TS ts)
{
    PetscErrorCode ierr;
    PetscReal time;
    Application *app;

    ierr = TSGetTime(ts, &time);
    CHKERRXX(ierr);
    ierr = TSGetApplicationContext(ts, &app);
    CHKERRXX(ierr);

    Model *model = (Model *)app->model;

    app->solverdata->copySolutionToMesh(app->mesh);

    for (unsigned int i = 0; i < app->mesh->num_faces; i++)
    {
        model->fnc_flux(app->mesh->faces[i], app);
    }

    app->timeStepNumber++;

    if (app->rw)
    {
        if ((app->timeStepNumber - app->lastOutputTimeStep) * app->outputSamplingRate >= 1)
        {
            app->lastOutputTimeStep = app->timeStepNumber;
            app->rw->write_state(time * app->p->t0 + app->simtime);
        }
    }

    TSType t;
    ierr = TSGetType(ts, &t);

    PetscPrintf(PETSC_COMM_WORLD, "%gs (%s)\n", time * app->p->t0, t);

    return 0;
}

void Application::transient(double duration, double dt)
{
    if (!this->mesh)
        throw std::runtime_error("mesh not set");
    PetscPrintf(PETSC_COMM_WORLD, "Starting BEuler...\n");
    PetscErrorCode ierr;

    SolverData *sd = (SolverData *)this->solverdata;
    sd->copyValuesFromMesh(this->mesh);

    dt = dt / this->p->t0; // in seconds

    ierr = TSSetType(sd->ts, TSBEULER);
    CHKERRXX(ierr);

    TSAdapt adapt;
    TSGetAdapt(sd->ts, &adapt);
    TSAdaptSetType(adapt, TSADAPTBASIC);

    // Starte die Simulation bei aktueller Zeit.
    PetscReal time;
    ierr = TSGetTime(sd->ts, &time);
    CHKERRXX(ierr);
    ierr = TSSetTimeStep(sd->ts, dt);
    CHKERRXX(ierr);
    ierr = TSSetSolution(sd->ts, sd->x);
    CHKERRXX(ierr);

    if (this->rw)
        this->rw->write_state(time * this->p->t0 + this->simtime);

    double final_time = duration / this->p->t0 + time; /* In seconds */
    ierr = TSSetMaxTime(sd->ts, final_time);
    CHKERRXX(ierr);
    PetscInt current_step;
    ierr = TSGetStepNumber(sd->ts, &current_step);
    CHKERRXX(ierr);
    ierr = TSSetMaxSteps(sd->ts, current_step + final_time / dt);
    CHKERRXX(ierr);

    ierr = TSSetExactFinalTime(sd->ts, TS_EXACTFINALTIME_MATCHSTEP);
    CHKERRXX(ierr);

    ierr = TSMonitorCancel(sd->ts);
    CHKERRXX(ierr);
    ierr = TSSetPostStep(sd->ts, ts_post_step);
    CHKERRXX(ierr);
    ierr = TSSetApplicationContext(sd->ts, (void *)this);
    CHKERRXX(ierr);

    ierr = TSSetPreStep(sd->ts, ts_pre_step);
    CHKERRXX(ierr);

    ierr = TSSetUp(sd->ts);
    CHKERRXX(ierr);
    ierr = TSSolve(sd->ts, sd->x);
    CHKERRXX(ierr);
    sd->copySolutionToMesh(this->mesh);
    if (this->rw)
        this->rw->write_state(duration + this->simtime);
}

PetscErrorCode ts_pre_step_cv(TS ts)
{
    PetscErrorCode ierr;
    PetscReal time;
    Application *app;
    ierr = TSGetTime(ts, &time);
    CHKERRXX(ierr);
    ierr = TSGetApplicationContext(ts, &app);
    CHKERRXX(ierr);

    PetscReal amplitude = app->CVAmplitude;
    PetscReal interval = app->CVAmplitude / app->CVScanRate * 2 / app->p->t0;

    PetscReal interval_time = fmod(time / interval, 1);

    PetscReal bias = 0;
    if (interval_time < 0.25)
    {
        bias = amplitude * interval_time * 2;
    }
    else if (interval_time < 0.75)
    {
        bias = (-amplitude * (interval_time - 0.25) + 0.25 * amplitude) * 2;
    }
    else
    {
        bias = (amplitude * (interval_time - 0.75) - 0.25 * amplitude) * 2;
    }

    app->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, app->p->num_species);
    app->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, app->p->num_species + 1);
    PetscPrintf(PETSC_COMM_WORLD, "%gs (pre step): %g\n", time * app->p->t0, bias);
    return 0;
}

void Application::cyclicVoltammetry(double scanRate, double amplitude)
{
    this->CVScanRate = scanRate;
    this->CVAmplitude = amplitude;

    double cycles = 2;
    int steps = 1e2;
    double dtmin = 1e-1 / this->p->t0;

    if (!this->mesh)
        throw std::runtime_error("mesh not set");
    PetscPrintf(PETSC_COMM_WORLD, "Starting cyclic voltammetry (BEuler)...\n");
    PetscErrorCode ierr;

    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, 0, this->p->num_species);
    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_RIGHT, 0, this->p->num_species);
    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, 0, this->p->num_species + 1);
    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_RIGHT, 0, this->p->num_species + 1);

    SolverData *sd = (SolverData *)this->solverdata;
    sd->copyValuesFromMesh(this->mesh);

    double duration = amplitude / scanRate * (2 * cycles) / this->p->t0;
    double dt = duration / steps / (2 * cycles);
    dt = std::min(dt, dtmin);

    ierr = TSSetType(sd->ts, TSBEULER);
    CHKERRXX(ierr);
    PetscReal time;
    ierr = TSGetTime(sd->ts, &time);
    CHKERRXX(ierr);
    ierr = TSSetTimeStep(sd->ts, dt);
    CHKERRXX(ierr);
    ierr = TSSetSolution(sd->ts, sd->x);
    CHKERRXX(ierr);

    double final_time = duration + time;
    ierr = TSSetMaxTime(sd->ts, final_time);
    CHKERRXX(ierr);
    PetscInt current_step;
    ierr = TSGetStepNumber(sd->ts, &current_step);
    CHKERRXX(ierr);
    ierr = TSSetMaxSteps(sd->ts, current_step + final_time / dt);
    CHKERRXX(ierr);

    ierr = TSSetExactFinalTime(sd->ts, TS_EXACTFINALTIME_STEPOVER);
    CHKERRXX(ierr);

    ierr = TSMonitorCancel(sd->ts);
    CHKERRXX(ierr);
    ierr = TSSetPostStep(sd->ts, ts_post_step);
    CHKERRXX(ierr); // Try PostStep instead of Monitor
    ierr = TSSetApplicationContext(sd->ts, (void *)this);
    CHKERRXX(ierr);

    ierr = TSSetPreStep(sd->ts, ts_pre_step_cv);
    CHKERRXX(ierr);

    ierr = TSSetUp(sd->ts);
    CHKERRXX(ierr);
    ierr = TSSolve(sd->ts, sd->x);
    CHKERRXX(ierr);
    sd->copySolutionToMesh(this->mesh);
}

PetscErrorCode ts_pre_step(TS ts)
{
    PetscErrorCode ierr;
    PetscReal time;
    Application *app;
    ierr = TSGetTime(ts, &time);
    CHKERRXX(ierr);
    ierr = TSGetApplicationContext(ts, &app);
    CHKERRXX(ierr);

    PetscPrintf(PETSC_COMM_WORLD, "%gs (pre step)\n", time * app->p->t0);

    return 0;
}

PetscErrorCode ts_pre_step_eis(TS ts)
{
    PetscErrorCode ierr;
    PetscReal time;
    Application *app;
    ierr = TSGetTime(ts, &time);
    CHKERRXX(ierr);
    ierr = TSGetApplicationContext(ts, &app);
    CHKERRXX(ierr);

    constexpr double pi = 3.14159265358979323846;

    PetscReal bias = app->EISAmplitude * sin(2 * pi * app->EISFrequency * (time - app->EISt0) * app->p->t0) + app->EISBias;

    app->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, app->p->num_species);
    app->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, bias, app->p->num_species + 1);
    PetscPrintf(PETSC_COMM_WORLD, "%gs (pre step): %g\n", time * app->p->t0, bias);
    return 0;
}

void Application::impedance(double frequency, double amplitude, double bias)
{
    this->EISFrequency = frequency;
    this->EISAmplitude = amplitude;
    this->EISBias = bias;

    double cycles = 2;
    int steps = 1e2;
    double dtmin = 1e-1 / this->p->t0;

    if (!this->mesh)
        throw std::runtime_error("mesh not set");
    PetscPrintf(PETSC_COMM_WORLD, "Starting impedance (BEuler)...\n");
    PetscErrorCode ierr;

    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_LEFT, 0, this->p->num_species);
    this->mesh->modify_boundary_state(FACETYPE_BOUNDARY_RIGHT, 0, this->p->num_species);

    SolverData *sd = (SolverData *)this->solverdata;
    sd->copyValuesFromMesh(this->mesh);

    double duration = 1 / frequency * cycles / this->p->t0;
    double dt = duration / steps / cycles;
    dt = std::min(dt, dtmin);

    ierr = TSSetType(sd->ts, TSBEULER);
    CHKERRXX(ierr);
    PetscReal time;
    ierr = TSGetTime(sd->ts, &time);
    CHKERRXX(ierr);
    ierr = TSSetTimeStep(sd->ts, dt);
    CHKERRXX(ierr);
    ierr = TSSetSolution(sd->ts, sd->x);
    CHKERRXX(ierr);

    this->EISt0 = time;

    double final_time = duration + time;
    ierr = TSSetMaxTime(sd->ts, final_time);
    CHKERRXX(ierr);
    PetscInt current_step;
    ierr = TSGetStepNumber(sd->ts, &current_step);
    CHKERRXX(ierr);
    ierr = TSSetMaxSteps(sd->ts, current_step + final_time / dt);
    CHKERRXX(ierr);
    ierr = TSSetExactFinalTime(sd->ts, TS_EXACTFINALTIME_STEPOVER);
    CHKERRXX(ierr);

    ierr = TSMonitorCancel(sd->ts);
    CHKERRXX(ierr);
    ierr = TSSetPostStep(sd->ts, ts_post_step);
    CHKERRXX(ierr);
    ierr = TSSetApplicationContext(sd->ts, (void *)this);
    CHKERRXX(ierr);

    ierr = TSSetPreStep(sd->ts, ts_pre_step_eis);
    CHKERRXX(ierr);

    ierr = TSSetUp(sd->ts);
    CHKERRXX(ierr);
    ierr = TSSolve(sd->ts, sd->x);
    CHKERRXX(ierr);
    sd->copySolutionToMesh(this->mesh);
}

double Application::getCellPotential(int index)
{
    Volume *vl = this->mesh->boundaries[FACETYPE_BOUNDARY_LEFT].front()->nvol[0];
    Volume *vr = this->mesh->boundaries[FACETYPE_BOUNDARY_RIGHT].front()->nvol[0];
    return vl->state[index] - vr->state[index];
}