#ifndef GMPNP_H
#define GMPNP_H

#include "Model.h"
#include <exception>

class GMPNP : public Model
{
public:
	void fnc_spec(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *);
	void fnc_pois(Volume *, PetscScalar *, PetscScalar *, Application *);
	void fnc_time(Volume *, PetscScalar *, PetscScalar *, PetscScalar *, unsigned int, Application *);
	void jac_spec_local(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *);
	void jac_pois_local(Volume *, PetscScalar *, PetscScalar *, Application *);
	void jac_time(Volume *, PetscScalar *, PetscScalar *, PetscScalar *, unsigned int i, Application *);
	void jac_spec_neighbour(Volume *, PetscScalar *, PetscScalar *, unsigned int, unsigned int, Application *);

	void jac_pois_neighbour(Volume *, PetscScalar *, PetscScalar *, unsigned int, Application *);

	void fnc_flux(Face *, Application *);

	std::vector<double> W_tilde;
	double L_debye = 0;
	double lambda_delta = 0;
	double epsilon_r_0 = 0;

	GMPNP(ParameterStore *p);
};

#endif
