#include "GMPNP.h"
#include <cmath>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <petscsys.h>

GMPNP::GMPNP(ParameterStore *p)
{
    this->W_tilde.resize(p->num_species, 0.0);
    this->epsilon_r_0 = 80.1;
    double M_water = 55;            // molarity of water Unit = M
    double epsilon_0 = 8.85419e-12; // vacum permitivity unit F/m
    this->L_debye = ((p->epsilon * p->debye) / epsilon_0);
    this->lambda_delta = 6 - this->epsilon_r_0;
    for (unsigned int s = 0; s < p->num_species; s++)
    {
        this->W_tilde[s] = p->hydration[s] / M_water;
    };
}

void GMPNP::fnc_time(Volume *v, PetscScalar *in, PetscScalar *din,
                     PetscScalar *dst, unsigned int i, Application *ctx)
{
    unsigned int vi = v->solution_index;
    /* Store transient term for i in dst */
    *dst = v->volume * din[vi + i];
}

void GMPNP::jac_time(Volume *v, PetscScalar *in,
                     PetscScalar *din, PetscScalar *dst, unsigned int i, Application *ctx)
{
    /* Calculate dF/dU_t for i and store it at dst */
    *dst = v->volume;
}

// dafe: checked except bv
void GMPNP::fnc_spec(Volume *v, PetscScalar *in, PetscScalar *dst,
                     unsigned int i, Application *ctx)
{
    unsigned int nos = v->num_states;
    unsigned int vi = v->solution_index;
    ParameterStore *p = ctx->p;

    if (!v->stateIsActive[i])
        return;

    PetscScalar c = in[vi + i];
    PetscScalar phi = in[vi + nos - 1];

    /* Flux term */
    PetscScalar flux = 0;
    for (unsigned int f = 0; f < v->num_faces; f++)
    {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];
        Volume *nv = v->nvol[f];
        Face *face = v->face[f];

        PetscScalar c_n, phi_n;
        unsigned int ni = nv->solution_index;

        phi_n = in[ni + nos - 1];
        c_n = in[ni + i];

        if (face->bcond_type[nos - 1] == 0)
            phi_n = face->boundary_states[nos - 1];
        if (face->bcond_type[i] == 0)
            c_n = face->boundary_states[i];

        PetscScalar c_f = 0.5 * (c + c_n);
        PetscScalar alpha = std::max(v->alpha, nv->alpha);

        if (face->bcond_type[i] == 2) // controlled flux boundary
        {
            for (auto &ecr : face->ecreactions)
            {
                if (!ecr->butler_volmer)
                    flux += A * ecr->fixed_current_density[i];
                else
                {
                    double bias = face->boundary_states[nos - 1];
                    double eta = (bias - phi) - ecr->e_eq;

                    double c_dep = (ecr->i_red != -1) ? in[vi + ecr->i_red] / ecr->c_ref_red : 1.0;

                    double j_ec = ecr->j0 * (c_dep * exp(-1 * ecr->alpha * eta));

                    PetscPrintf(PETSC_COMM_WORLD, "eta: %g, j_ec: %g \n", eta, j_ec);

                    // currently, only reduction is studied.

                    flux += ecr->nu[i] * A * j_ec / p->F / ecr->z;
                }
            }
        }
        else if (!nv->stateIsActive[i])
            flux += 0;
        else
        {
            double sum_c_volume_term = 0;
            double sum_c_volume_gradient = 0;
            for (unsigned int s = 0; s < p->num_species; s++)
            {
                PetscScalar c_p_volume_term, c_n_volume_term, c_f_volume_term;
                c_p_volume_term = in[vi + s];
                c_n_volume_term = in[ni + s];
                if (face->bcond_type[i] == 0)
                    c_n_volume_term = face->boundary_states[s];
                c_f_volume_term = 0.5 * (c_p_volume_term + c_n_volume_term);
                sum_c_volume_gradient += p->gmpnp_nu[s] * (c_n_volume_term - c_p_volume_term);
                sum_c_volume_term += p->gmpnp_nu[s] * c_f_volume_term;
            };

            flux += A / ksi * alpha * p->D[i] * ((c_n - c) + p->z[i] * c_f * (phi_n - phi) + (p->beta[i] * c_f * (sum_c_volume_gradient / (1 - sum_c_volume_term))));
        }
    }

    PetscScalar source = 0;

    /* Mass Action Law Reaction */
    for (unsigned int j = 0; j < p->num_reactions; j++)
    {
        double RcoeffMA = (p->mu[i][j] + p->nu[i][j]) * v->volume;
        double p_f = 1.0, p_b = 1.0;
        for (unsigned int m = 0; m < p->num_species; m++)
        {
            p_f *= pow(in[vi + m], p->mu[m][j]);
            p_b *= pow(in[vi + m], -p->nu[m][j]);
        }
        source += RcoeffMA * (p->k_f[j] * p_f - p->k_b[j] * p_b);
    }

    *dst = 0 - flux + source;
}

void GMPNP::jac_spec_local(Volume *v, PetscScalar *in,
                           PetscScalar *dst, unsigned int i, Application *ctx)
{
    unsigned int nos = v->num_states;
    unsigned int vi = v->solution_index;
    ParameterStore *p = ctx->p;

    if (!v->stateIsActive[i])
        return;

    PetscScalar c = in[vi + i];
    PetscScalar phi = in[vi + nos - 1];

    PetscScalar dc = 0;
    PetscScalar dphi = 0;
    unsigned int f;
    for (f = 0; f < v->num_faces; f++)
    {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];

        Volume *nv = v->nvol[f];
        Face *face = v->face[f];
        unsigned int ni = nv->solution_index;
        PetscScalar c_n, phi_n, c_f;

        phi_n = in[ni + nos - 1];
        c_n = in[ni + i];

        if (face->bcond_type[nos - 1] == 0)
            phi_n = face->boundary_states[nos - 1];
        if (face->bcond_type[i] == 0)
            c_n = face->boundary_states[i];

        c_f = 0.5 * (c + c_n);
        PetscScalar alpha = std::max(v->alpha, nv->alpha);

        PetscScalar factor = A * alpha * p->D[i] / ksi;
        PetscScalar sum_c_volume_gradient = 0;
        PetscScalar sum_c_volume_term = 0;
        for (unsigned int s = 0; s < p->num_species; s++)
        {
            PetscScalar c_p_volume_term, c_n_volume_term, c_f_volume_term;
            c_p_volume_term = in[vi + s];
            c_n_volume_term = in[ni + s];
            if (face->bcond_type[i] == 0)
                c_n_volume_term = face->boundary_states[s];
            c_f_volume_term = 0.5 * (c_p_volume_term + c_n_volume_term);
            sum_c_volume_gradient += p->gmpnp_nu[s] * (c_n_volume_term - c_p_volume_term);
            sum_c_volume_term += p->gmpnp_nu[s] * c_f_volume_term;
        };

        if ((face->bcond_type[i] == -1 || face->bcond_type[i] == 0) && nv->stateIsActive[i])
        {
            for (unsigned int s = 0; s < p->num_species; s++)
            {
                if (s == i)
                {
                    dc += factor * (1 - p->z[i] * 0.5 * (phi_n - phi)) - (p->beta[i] * factor * 0.5 * (((1) * (sum_c_volume_gradient / (1 - (sum_c_volume_term)))) + (c + c_n) * ((-p->gmpnp_nu[i] * (1 - (sum_c_volume_term)) + (p->gmpnp_nu[i] * 0.5 * sum_c_volume_gradient)) / ((1 - (sum_c_volume_term)) * (1 - (sum_c_volume_term))))));
                }
                else
                {
                    dc += -(p->beta[i] * factor * 0.5 * (((0) * (sum_c_volume_gradient / (1 - (sum_c_volume_term)))) + (c + c_n) * ((-p->gmpnp_nu[s] * (1 - (sum_c_volume_term)) + (p->gmpnp_nu[s] * 0.5 * sum_c_volume_gradient)) / ((1 - (sum_c_volume_term)) * (1 - (sum_c_volume_term))))));
                }
            };
            dphi += factor * p->z[i] * c_f;
        }
        if (face->bcond_type[i] == 2)
        {
            for (auto &ecr : face->ecreactions)
            {
                if (ecr->butler_volmer)
                {
                    double bias = face->boundary_states[nos - 1];
                    double eta = (bias - phi) - ecr->e_eq;

                    if (ecr->i_red != -1)
                        dst[ecr->i_red] += -1 * ecr->nu[i] * A / p->F / ecr->z * ecr->j0 * (1 / ecr->c_ref_red * exp(-1 * ecr->alpha * eta));

                    double c_dep = (ecr->i_red != -1) ? in[vi + ecr->i_red] / ecr->c_ref_red : 1.0;

                    double j_ec = ecr->j0 * (c_dep * exp(-1 * ecr->alpha * eta));

                    dphi += -1 * ecr->nu[i] * A / p->F / ecr->z * j_ec * ecr->alpha;
                }
            }
        }
    }

    dst[i] += dc;
    dst[nos - 1] = dphi;

    /* Mass Action Reaction */
    for (unsigned int k = 0; k < p->num_species; k++)
    {
        double ds = 0;
        for (unsigned int j = 0; j < p->num_reactions; j++)
        {
            double RcoeffMA = (p->mu[i][j] + p->nu[i][j]) * v->volume;
            PetscScalar p_f = 1.0, p_b = 1.0;

            for (unsigned int m = 0; m < p->num_species; m++)
            {
                if (k == m)
                {
                    if (p->mu[m][j] > 1)
                        p_f *= pow(in[vi + m], p->mu[m][j] - 1);
                    if (-p->nu[m][j] > 1)
                        p_b *= pow(in[vi + m], -p->nu[m][j] - 1);
                }
                else
                {
                    p_f *= pow(in[vi + m], p->mu[m][j]);
                    p_b *= pow(in[vi + m], -p->nu[m][j]);
                }
            }
            ds += RcoeffMA * (p->k_f[j] * p->mu[k][j] * p_f - p->k_b[j] * (-p->nu[k][j]) * p_b);
        }
        dst[k] += ds;
    }
}

void GMPNP::jac_spec_neighbour(Volume *v, PetscScalar *in,
                               PetscScalar *dst, unsigned int i, unsigned int f, Application *ctx)
{
    unsigned int nos = v->num_states; // instead of num _states , num_speciese could be used
    unsigned int vi = v->solution_index;
    Volume *nv = v->nvol[f];
    ParameterStore *p = ctx->p;
    Face *face = v->face[f];

    if (!v->stateIsActive[i])
        return;
    if (!nv->stateIsActive[i])
        return;

    if (face->bcond_type[i] == -1)
    {

        PetscScalar dphi = 0, dc = 0;
        unsigned int ni = nv->solution_index;

        PetscScalar c, phi, c_n, phi_n, c_f;
        phi = in[vi + nos - 1];
        phi_n = in[ni + nos - 1];

        c = in[vi + i];
        c_n = in[ni + i];
        c_f = 0.5 * (c + c_n);

        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];
        PetscScalar alpha = std::max(v->alpha, nv->alpha);

        PetscScalar factor = A * alpha * p->D[i] / ksi;
        PetscScalar sum_c_volume_gradient = 0;
        PetscScalar sum_c_volume_term = 0;
        for (unsigned int s = 0; s < p->num_species; s++)
        {
            PetscScalar c_p_volume_term, c_n_volume_term, c_f_volume_term;
            c_p_volume_term = in[vi + s];
            c_n_volume_term = in[ni + s];
            if (face->bcond_type[i] == 0)
                c_n_volume_term = face->boundary_states[s];
            c_f_volume_term = 0.5 * (c_p_volume_term + c_n_volume_term);
            sum_c_volume_gradient += p->gmpnp_nu[s] * (c_n_volume_term - c_p_volume_term);
            sum_c_volume_term += p->gmpnp_nu[s] * c_f_volume_term;
        };

        for (unsigned int s = 0; s < p->num_species; s++)
        {
            if (s == i)
            {
                dc += -factor * (1 + p->z[i] * 0.5 * (phi_n - phi)) - (p->beta[i] * factor * 0.5 * ((1) * (sum_c_volume_gradient / (1 - (sum_c_volume_term))) + (c + c_n) * ((p->gmpnp_nu[i] * (1 - (sum_c_volume_term)) + (p->gmpnp_nu[i] * 0.5 * sum_c_volume_gradient)) / ((1 - (sum_c_volume_term)) * (1 - (sum_c_volume_term))))));
            }
            else
            {
                dc += -1 * (p->beta[i] * factor * 0.5 * ((0) * (sum_c_volume_gradient / (1 - (sum_c_volume_term))) + (c + c_n) * ((p->gmpnp_nu[s] * (1 - (sum_c_volume_term)) + (p->gmpnp_nu[s] * 0.5 * sum_c_volume_gradient)) / ((1 - (sum_c_volume_term)) * (1 - (sum_c_volume_term))))));
            }
        };
        dst[i] += dc;

        dphi += -factor * p->z[i] * c_f;
        dst[nos - 1] += dphi;
    }
}

void GMPNP::fnc_pois(Volume *v, PetscScalar *in, PetscScalar *dst,
                     Application *ctx)
{
    unsigned int vi = v->solution_index;
    unsigned int nos = v->num_states;
    ParameterStore *p = ctx->p;

    /* Source term */
    PetscScalar rho = v->charge, source = 0.0;
    for (unsigned int i = 0; i < nos - 1; i++)
    {
        if (v->stateIsActive[i])
            rho += in[vi + i] * p->z[i];
    }

    PetscScalar phi = in[vi + nos - 1];
    PetscScalar flux = 0;

    source = v->volume * rho * this->L_debye;

    /* Flux term */
    for (unsigned int f = 0; f < v->num_faces; f++)
    {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];

        Volume *nv = v->nvol[f];
        Face *face = v->face[f];
        unsigned int ni = nv->solution_index;
        PetscScalar phi_n = 0;
        if (face->bcond_type[nos - 1] == 0)
        {
            phi_n = face->boundary_states[nos - 1];
        }
        else
        {
            phi_n = in[ni + nos - 1];
        }

        PetscScalar sum_of_cations_con = 0;
        for (unsigned int s = 0; s < p->num_species; s++)
        {
            if (p->z[s] > 0)
            {
                PetscScalar c, c_n;
                c = in[vi + s];
                c_n = in[ni + s];
                if (face->bcond_type[s] == 0) // added
                {
                    c_n = face->boundary_states[s];
                }
                sum_of_cations_con += this->W_tilde[s] * 0.5 * (c + c_n);
            };
        };

        if (face->bcond_type[nos - 1] == 2)
            flux += 0;
        else if (face->bcond_type[nos - 1] == 3)
        {
            double bias = face->boundary_states[nos - 1];
            double C_stern = (this->epsilon_r_0 + this->lambda_delta * sum_of_cations_con) / p->L_stern;
            flux += A * C_stern * (bias - phi);
        }
        else
        {
            flux += A / ksi * (phi_n - phi) * (this->epsilon_r_0 + this->lambda_delta * sum_of_cations_con);
        };
    }
    *dst = flux + source;
}

void GMPNP::jac_pois_local(Volume *v, PetscScalar *in,
                           PetscScalar *dst, Application *ctx)
{
    ParameterStore *p = ctx->p;
    unsigned int nos = v->num_states;

    PetscScalar dphi = 0;
    PetscScalar dc = 0;
    PetscScalar dc_faces = 0;
    for (unsigned int f = 0; f < v->num_faces; f++)
    {
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];
        Face *face = v->face[f];

        Volume *nv = v->nvol[f];
        unsigned int vi = v->solution_index;
        unsigned int ni = nv->solution_index;
        PetscScalar sum_of_cations_con = 0;
        for (unsigned int s = 0; s < p->num_species; s++)
        {
            if (p->z[s] > 0)
            {
                PetscScalar c, c_n;
                c = in[vi + s];
                c_n = in[ni + s];
                if (face->bcond_type[s] == 0)
                {
                    c_n = face->boundary_states[s];
                }
                sum_of_cations_con += this->W_tilde[s] * 0.5 * (c + c_n);
            };
        };

        if (face->bcond_type[nos - 1] == 2)
            dphi += 0;
        else if (face->bcond_type[nos - 1] == 3)
        {
            double C_stern = (this->epsilon_r_0 + this->lambda_delta * sum_of_cations_con) / p->L_stern;
            dphi = A * C_stern * -1;
        }
        else
        {
            dphi += (-A / ksi) * (this->epsilon_r_0 + (this->lambda_delta * sum_of_cations_con));
        };
    }
    dst[nos - 1] = dphi;

    for (unsigned int i = 0; i < nos - 1; i++)
    {
        if (v->stateIsActive[i])
        {
            if (p->z[i] > 0)
            {
                PetscScalar phi, phi_n;
                for (unsigned int f = 0; f < v->num_faces; f++)
                {
                    Volume *nv = v->nvol[f];
                    unsigned int vi = v->solution_index;
                    unsigned int ni = nv->solution_index;
                    Face *face = v->face[f];
                    PetscScalar A = v->face[f]->area;
                    PetscScalar ksi = v->distance[f];
                    phi = in[vi + nos - 1];
                    phi_n = in[ni + nos - 1];
                    if (face->bcond_type[nos - 1] == 0)
                    {
                        phi_n = face->boundary_states[nos - 1];
                    }
                    if (face->bcond_type[nos - 1] == 3)
                    {
                        double bias = face->boundary_states[nos - 1];
                        dc_faces += this->lambda_delta * this->W_tilde[i] / p->L_stern * A * (bias - phi);
                    }
                    else
                        dc_faces += (-A * 0.5 / ksi) * this->lambda_delta * this->W_tilde[i] * (phi_n - phi);
                }
            }
            dc = (v->volume * this->L_debye * p->z[i]);
        }
        dst[i] = dc + dc_faces;
    }
}

void GMPNP::jac_pois_neighbour(Volume *v, PetscScalar *in,
                               PetscScalar *dst, unsigned int f, Application *ctx)
{
    Face *face = v->face[f];
    Volume *nv = v->nvol[f];
    unsigned int nos = nv->num_states;

    if (face->bcond_type[nos - 1] != -1)
    {
        dst[nos - 1] += 0;
    }
    else
    {
        ParameterStore *p = ctx->p;
        unsigned int vi = v->solution_index;
        unsigned int ni = nv->solution_index;
        PetscScalar A = v->face[f]->area;
        PetscScalar ksi = v->distance[f];
        PetscScalar sum_of_cations_con = 0;
        for (unsigned int s = 0; s < p->num_species; s++)
        {
            if (p->z[s] > 0)
            {
                PetscScalar c, c_n;
                c = in[vi + s];
                c_n = in[ni + s];
                if (face->bcond_type[s] == 0) // added
                {
                    c_n = face->boundary_states[s];
                }
                sum_of_cations_con += W_tilde[s] * 0.5 * (c + c_n);
                if (v->stateIsActive[s])
                {
                    PetscScalar phi = in[vi + nos - 1];
                    PetscScalar phi_n = in[ni + nos - 1];
                    dst[s] = (A * 0.5 / ksi) * this->lambda_delta * this->W_tilde[s] * (phi_n - phi);
                }
            }
        }
        dst[nos - 1] = (A / ksi) * (this->epsilon_r_0 + (this->lambda_delta * sum_of_cations_con));
    }
}

void GMPNP::fnc_flux(Face *face, Application *ctx)
{
    Volume *v1, *v2;
    v1 = face->nvol[0];
    v2 = face->nvol[1];
    if (!v1 || !v2)
        throw std::runtime_error("A face is not properly connected");

    ParameterStore *p = ctx->p;
    unsigned int nos = v1->num_states;

    PetscScalar ksi = v2->center.x - v1->center.x;
    PetscScalar dphi, dc, c_f;
    dphi = (v2->state[nos - 1] - v1->state[nos - 1]);
    PetscScalar alpha = std::max(v1->alpha, v2->alpha);

    face->flux[nos - 1] = 0;

    for (unsigned int i = 0; i < nos - 1; i++)
    {
        dc = v2->state[i] - v1->state[i];
        c_f = 0.5 * (v2->state[i] + v1->state[i]);

        if (!(v1->stateIsActive[i] && v2->stateIsActive[i]))
            face->flux[i] = 0;
        else if (face->bcond_type[i] == 2)
        {
            face->flux[i] = 0;
            int direction = v1->center.x > v2->center.x ? 1 : -1;
            for (auto &ecr : face->ecreactions)
            {
                if (!ecr->butler_volmer)
                    face->flux[i] += ecr->fixed_current_density[i] * direction;
                else
                {

                    Volume *v = v1->isGhostNode ? v2 : v1;

                    double phi = v->state[nos - 1];

                    double bias = face->boundary_states[nos - 1];
                    double eta = bias - phi - ecr->e_eq;

                    double c_dep = (ecr->i_red != -1) ? v->state[ecr->i_red] / ecr->c_ref_red : 1.0;
                    double j_ec = ecr->j0 * (c_dep * exp(-1 * ecr->alpha * eta));

                    // currently, only reduction is studied.
                    // - c_red / ecr->c_ref_red * exp(-1 * (1 - ecr->alpha) * eta * ecr->z));

                    face->flux[i] += ecr->nu[i] * j_ec / p->F / ecr->z * direction;
                }
            }
        }
        else if (face->bcond_type[i] != -1)
        {
            face->flux[i] += 0;
        }
        else
        {
            face->flux[i] = 0;
            double sum_c_volume_term = 0;
            double sum_c_volume_gradient = 0;
            for (unsigned int s = 0; s < p->num_species; s++)
            {
                PetscScalar dc_steric, c_f_steric;
                dc_steric = v2->state[s] - v1->state[s];
                c_f_steric = 0.5 * (v2->state[s] + v1->state[s]);
                sum_c_volume_gradient += p->gmpnp_nu[s] * (dc_steric);
                sum_c_volume_term += p->gmpnp_nu[s] * c_f_steric;
            }

            face->flux[i] = -alpha * p->D[i] / ksi * (dc + p->z[i] * c_f * dphi + (c_f * p->beta[i] * (sum_c_volume_gradient / (1 - sum_c_volume_term))));
        }
        face->flux[nos - 1] += face->flux[i] * p->z[i];
    }
}