![](./enpen_logo.svg)

# EC-EnPEn

EC-EnPEn solves the general modified Poisson-Nernst-Planck equations at elecrodes and with (electrochemical) reactions. 

### Installation

- Install PETSc https://petsc.org/release/install/
- Install Python dependencies: ```pip3 install -r py/requirements.txt```

### Usage

- Run with ```make cathode-run```
- View results with ```./GUI.py cathode```

